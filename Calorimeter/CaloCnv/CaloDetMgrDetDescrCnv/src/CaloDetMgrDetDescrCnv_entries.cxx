/*
  Copyright (C) 2002-2018 CERN for the benefit of the ATLAS collaboration
*/
#include "CaloDetMgrDetDescrCnv/CaloMgrDetDescrCnv.h"
#include "CaloDetMgrDetDescrCnv/CaloSuperCellMgrDetDescrCnv.h"

DECLARE_CONVERTER( CaloMgrDetDescrCnv )
DECLARE_CONVERTER( CaloSuperCellMgrDetDescrCnv )

